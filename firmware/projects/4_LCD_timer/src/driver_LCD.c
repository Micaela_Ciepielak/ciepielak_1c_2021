/*
 * Electrónica Programable
 * FIUNER - 2021
 * Autora: Micaela Ciepielak
 */

/*==================[inclusions]=============================================*/
#include "../../4_LCD_timer/inc/driver_LCD.h"       /* <= own header */

#include "../../../modules/lpc4337_m4/drivers_devices/inc/DisplayITS_E0803.h"
#include "gpio.h"
#include "hc_sr4.h"
#include "led.h"
#include "systemclock.h"
#include "switch.h"
#include "timer.h"
#include "delay.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/
#define MASC 0x01

gpio_t pins[7] = { GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1,
		GPIO_3, GPIO_5 };
static unsigned char estado1 = 0;
static unsigned char estado2 = 0;

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
void PrenderLedsPorDistancia(uint16_t dist) {
	if (dist < 10) {
		LedOn(LED_RGB_B);
		LedOff(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);
	}
	if (dist < 20 && dist > 10) {
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);
	}
	if (dist < 30 && dist > 20) {
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOn(LED_2);
		LedOff(LED_3);
	}
	if (dist > 30) {
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOn(LED_2);
		LedOn(LED_3);

	}
}

void Tecla1() {
	estado1 = estado1 + 1;
//		DelayUs(600);

}

void Tecla2() {
	estado2 = estado2 + 1;
//		DelayUs(600);
}

void MostrarPorPantallaDistancia() {

	uint16_t distancia;
	if (estado1 & MASC) {
		distancia = HcSr04ReadDistanceCentimeters();

		if (estado2 & MASC) {
		} else {
			ITSE0803DisplayValue(distancia);
			PrenderLedsPorDistancia(distancia);
		}
	}

	else {
		ITSE0803DisplayValue(0);

	}
	DelayUs(300);

}

timer_config my_timer = { TIMER_A, 100, &MostrarPorPantallaDistancia };

void SisInit(void) {
	ITSE0803Init(pins);
	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);
	LedsInit();
	SystemClockInit();
	SwitchesInit();
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
}

int main(void) {
	SisInit();
	uint8_t teclas;
	teclas = SwitchesRead();

	SwitchActivInt(1, Tecla1);
	SwitchActivInt(2, Tecla2);

	while (1);
	return 0;
}

/*==================[end of file]============================================*/

