/*
 * Electrónica Programable
 * FIUNER - 2021
 * Autor/es: Micaela Ciepielak
 */

/*==================[inclusions]=============================================*/
#include "../inc/3_blinking_switch.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/
#define COUNT_DELAY 3000000
/*==================[internal data definition]===============================*/

void Delay(void) {
	uint32_t i;

	for (i = COUNT_DELAY; i != 0; i--) {
		asm ("nop");
	}
}

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/
#define MASC 0x01

/*==================[external functions definition]==========================*/

int main(void) {

	// configuracion
	uint8_t teclas;
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	// bucle infinito
	static unsigned char estado = 0;

	while (1) {
		teclas = SwitchesRead();
		switch (teclas) {
		case SWITCH_1:
			// cuando se apreta tarda en
			estado = estado + 1;
			if (estado & MASC) {
				LedOn(LED_1);
				LedOn(LED_2);
				LedOn(LED_3);

			} else {
				LedOff(LED_1);
				LedOff(LED_2);
				LedOff(LED_3);
			}

			break;
		case SWITCH_2:
			LedOn(LED_1);
			Delay();
			LedOff(LED_1);
			Delay();
			break;
		case SWITCH_3:
			LedOn(LED_2);
			Delay();
			LedOff(LED_2);
			Delay();
			break;
		case SWITCH_4:
			LedOn(LED_3);
			Delay();
			LedOff(LED_3);
			Delay();
			break;
		}
	}

}

/*==================[end of file]============================================*/

