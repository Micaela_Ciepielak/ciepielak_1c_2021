/*! @mainpage 3_blinking_switch
 *
 * \section Descripcion General
 *  Apretando la tecla 1 se mantienen encendidos los led 1, 2 y 3.
 *  Si se la vuelve a apretar, se apagan.
 *  La tecla 2 blinquea el Led 1, la tecla 3 blinquea el Led 2 y la tecla 4 blinquea el Led 3
 *
 *
 * \section hardConn Coneccion de Hardware
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	----	 	| 	--------	|
 *
 * @section changelog Changelog
 *
 * |   Fecha    | Descripcion                                    |
 * |:----------:|:-----------------------------------------------|
 * | 20/04/2021	| Creacion del proyecto    						 |
 * | 14/05/2021 | Creacion de la documentacion	                 |
 *
 * @author Micaela Ciepielak
 *
 */

#ifndef _BLINKING_SWITCH_H
#define _BLINKING_SWITCH_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

