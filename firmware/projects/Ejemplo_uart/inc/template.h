/*! @mainpage Ejemplo_uart
 *
 * \section Descripcion General
 *
 *	Proyecto para probar las funciones de la libreria uart.h
 *
 *
 * @section changelog Changelog
 *
 * |   Fecha    | Descripcion                                    |
 * |:----------:|:-----------------------------------------------|
 * | 14/04/2020 | Creacion del proyecto			                 |
 * | 14/05/2020 | Creacion de la documentacion	                 |
 * | 			| 	                     						 |
 *
 * @author Micaela Ciepielak
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

