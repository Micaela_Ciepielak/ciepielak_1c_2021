/*
 * Electrónica Programable
 * FIUNER - 2021
 * Autora: Micaela Ciepielak
 */


/*==================[inclusions]=============================================*/
#include "template.h"       /* <= own header */
#include "uart.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

void DoPuerto(){
	 uint8_t dato;
	 UartReadByte(SERIAL_PORT_PC, &dato);
	if(dato == 'a'){
		a = a + 200;
	}
}



int main(void){
	serial_config serial_int= { SERIAL_PORT_PC , 115200 , Dopuerto};
	UartInit(&serial_int);

	while (1){
		 serial_config serial= { SERIAL_PORT_PC , 115200 , NO_INT};  //interrupcion NO_INT = no se usa interrupcion
		UartInit(&serial);
		UartSendString(SERIAL_PORT_PC, UartItoa(a,10));
		UartSendString(SERIAL_PORT_PC, "Hola Mundo");
		DelaySec();
		a++;
	}
    
	return 0;
}

/*==================[end of file]============================================*/

