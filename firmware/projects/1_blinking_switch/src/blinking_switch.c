/*
 * Electrónica Programable
 * FIUNER - 2021
 * Autora: Micaela Ciepielak
 */

/*==================[inclusions]=============================================*/
#include "blinking_switch.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/
#define COUNT_DELAY 3000000
/*==================[internal data definition]===============================*/

void Delay(void) {
	uint32_t i;

	for (i = COUNT_DELAY; i != 0; i--) {
		asm ("nop");
	}
}

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void) {

	// configuracion
	uint8_t teclas;
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	LedOn(LED_3);
	// bucle infinito
	while (1) {
		teclas = SwitchesRead();
		switch (teclas) {
		case SWITCH_2:
			LedOn(LED_1);
			Delay();
			LedOff(LED_1);
			Delay();
			break;
		case SWITCH_3:
			LedOn(LED_2);
			Delay();
			LedOff(LED_2);
			Delay();
			break;
		}
	}

}

/*==================[end of file]============================================*/

