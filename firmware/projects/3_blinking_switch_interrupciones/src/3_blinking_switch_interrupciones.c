/*
 * Electrónica Programable
 * FIUNER - 2021
 * Autora: Micaela Ciepielak
 */

/*==================[inclusions]=============================================*/
#include "../inc/3_blinking_switch_interrupciones.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/
#define COUNT_DELAY 3000000

#define MASC 0x01
/*==================[internal data definition]===============================*/

void Delay(void) {
	uint32_t i;

	for (i = COUNT_DELAY; i != 0; i--) {
		asm ("nop");
	}
}

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

void LedBlinking(void) {
	static unsigned char estado = 0;
	estado = estado + 1;
	if (estado & MASC) {
		LedOn(LED_1);
		LedOn(LED_2);
		LedOn(LED_3);

	} else {
		LedOff(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);
	}
}
int main(void) {

	// configuracion

	SystemClockInit();
	LedsInit();
	SwitchesInit();
	LedOn(LED_3);
	SwitchActivInt(SWITCH_1, LedBlinking);
	//static unsigned char estado=0;

}

/*==================[end of file]============================================*/

