/*! @mainpage 5_Osciloscopio_ECG
 *
 * \section Descripcion General
 *
 *
 *	Aplicación que utiliza una señal digital de ECG y la convierte en una señal analógica. Luego se la visualiza utilizando un
 *	osciloscopio ya implementado.
 * 	Aclaración: como se tiene una señal de ECG precargada, se debe puentear en la EDU-CIAA, conectando la salida del DAC a la entrada CH1 del AD.
 *
 *
 * \section hardConn Coneccion de Hardware
 *
 *
 *
 *	Coneccion de Hardware:
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 		PIN1 	| 	CH1			|
 * | 		PIN1 	| 	DAC			|
 * | 		+5V	 	| 	+5V			|
 * | 		GND	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Fecha    | Descripcion                                    |
 * |:----------:|:-----------------------------------------------|
 * | 21/05/2021 | Creacion del proyecto			                 |
 * | 04/06/2021 | Creacion de la documentacion	                 |
 * | 			| 	                     						 |
 *
 * @author Micaela Ciepielak
 *
 */

#ifndef _BLINKING_SWITCH_H
#define _BLINKING_SWITCH_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

