/*! @mainpage 4_LCD_uart
 *
 * \section Descripcion General
 *
 *
 *	Aplicacion que muestra distancia medida utilizando los leds de la siguiente manera:
 *	Si la distancia está entre 0 y 10 cm, encender el LED_RGB_B (Azul).
 *	Si la distancia está entre 10 y 20 cm, encender el LED_RGB_B (Azul) y LED_1.
 *	Si la distancia está entre 20 y 30 cm, encender el LED_RGB_B (Azul), LED_1 y LED_2
 *	Si la distancia es mayor a 30 cm, encender el LED_RGB_B (Azul), LED_1, LED_2 y LED_3.
 *	Ademas muestra el valor de distancia en cm utilizando el display LCD.
 *	Usa 'o' para activar y detener la medición.
 *	Usa 'h' para mantener el resultado (“HOLD”).
 *
 *	Se conecta la EDU-CIAA con la PC mediante el bridge UART-USB y luego envia, por este canal de comunicación,
 *	los datos de medida obtenidos del sensor de distancia ultrasónico HC-SR04.
 *	Utiliza los drivers de timer.h y uart.h
 *
 *
 * \section hardConn Coneccion de Hardware
 *
 *	Coneccion de Hardware del LCD:
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 		D1	 	| 	LCD1		|
 * | 		D2	 	| 	LCD2		|
 * |	 	D3	 	| 	LCD3		|
 * |	 	D4	 	| 	LCD4		|
 * |	 	SEL_0	| 	GPIO1		|
 * | 		SEL_0	| 	GPIO3		|
 * |	 	SEL_0	| 	GPIO5		|
 * | 		+5V	 	| 	+5V			|
 * | 		GND	 	| 	GND			|
 *
 *
 * 	Coneccion de Hardware del sensor por ultrasonido:
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 		ECHO 	| 	T_FILE2		|
 * | 		TRIGGER	| 	T_FILE3		|
 * | 		+5V	 	| 	+5V			|
 * | 		GND	 	| 	GND			|
 *
 * @section changelog Changelog
 *
 * |   Fecha    | Descripcion                                    |
 * |:----------:|:-----------------------------------------------|
 * | 14/04/2021 | Creacion del proyecto			                 |
 * | 14/05/2021 | Creacion de la documentacion	                 |
 * | 			| 	                     						 |
 *
 * @author Micaela Ciepielak
 *
 */
#ifndef _BLINKING_SWITCH_H
#define _BLINKING_SWITCH_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

