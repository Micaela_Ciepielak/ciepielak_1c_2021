/*
 * Electrónica Programable
 * FIUNER - 2021
 * Autora: Micaela Ciepielak
 */

/*==================[inclusions]=============================================*/
#include "../../4_LCD_uart/inc/driver_LCD.h"       /* <= own header */

#include "../../../modules/lpc4337_m4/drivers_devices/inc/DisplayITS_E0803.h"
#include "gpio.h"
#include "hc_sr4.h"
#include "led.h"
#include "systemclock.h"
#include "timer.h"
#include "delay.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/
#define MASC 0x01

gpio_t pins[7] = { GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1,
		GPIO_3, GPIO_5 };
static unsigned char estado1 = 0;
static unsigned char estado2 = 0;

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
void PrenderLedsPorDistancia(uint16_t dist) {
	if (dist < 10) {
		LedOn(LED_RGB_B);
		LedOff(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);
	}
	if (dist < 20 && dist > 10) {
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);
	}
	if (dist < 30 && dist > 20) {
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOn(LED_2);
		LedOff(LED_3);
	}
	if (dist > 30) {
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOn(LED_2);
		LedOn(LED_3);

	}
}

void MostrarPorPantallaDistancia() {

	uint16_t distancia;

	if (estado1 & MASC) {
		distancia = HcSr04ReadDistanceCentimeters();

		if (estado2 & MASC) {
		} else {
			UartSendString(SERIAL_PORT_PC, UartItoa(distancia, 10));
			UartSendString(SERIAL_PORT_PC, "cm\n\r");
			ITSE0803DisplayValue(distancia);
			PrenderLedsPorDistancia(distancia);
		}
	}

	else {
		ITSE0803DisplayValue(0);

	}
	DelayUs(300);

}

void OnOff(uint8_t tecla) {

	if (tecla == 'o') {
		estado1 = estado1 + 1;
	}

}

void Hold(uint8_t tecla) {

	uint8_t estado2 = 0;
	if (tecla == 'h') {
		estado2 = estado2 + 1;
	}

}

void FuncionUart(void) {
	uint8_t tecla;
	UartReadByte(SERIAL_PORT_PC, &tecla);

	OnOff(tecla);

	Hold(tecla);

}

timer_config my_timer = { TIMER_A, 1000, &MostrarPorPantallaDistancia };

void SisInit(void) {
	ITSE0803Init(pins);
	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);
	LedsInit();
	SystemClockInit();
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
}

serial_config serial = { SERIAL_PORT_PC, 115200, FuncionUart }; //interrupcion NO_INT = no se usa interrupcion

int main(void) {

	SisInit();

	UartInit(&serial);

	return 0;
}

/*==================[end of file]============================================*/

