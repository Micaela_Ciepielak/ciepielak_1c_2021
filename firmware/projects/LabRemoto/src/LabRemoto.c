/*
 * Electrónica Programable
 * FIUNER - 2021
 * Autora: Micaela Ciepielak
 */



/*==================[inclusions]=============================================*/
#include "../inc/LabRemoto.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "gpio.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	LedsInit();		//inicializa puertos
	gpio_t puerto = GPIO_T_COL1;
	GPIOInit(puerto, GPIO_INPUT);

    while(1)
     {
    	while ( GPIORead(puerto) == 1 ){
    	LedOn(LED_2);
    	}
    	while ( GPIORead(puerto) == 0){
    	LedOff(LED_2);
    	}
	}
    
	return 0;
}
/*==================[end of file]============================================*/

