/*! @mainpage LabRemoto
 *
 * \section Descripcion General
 *
 *	Aplicacion que prueba el laboratorio remoto
 *
 *
 * \section hardConn Coneccion de Hardware
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO_T_COL1	|
 *
 * @section changelog Changelog
 *
 * |   Fecha    | Descripcion                                    |
 * |:----------:|:-----------------------------------------------|
 * | 05/05/2020 | Creacion de la documentacion	                 |
 * | 			| 	                     						 |
 *
 * @author Micaela Ciepielak
 *
 */

#ifndef _BLINKING_SWITCH_H
#define _BLINKING_SWITCH_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

