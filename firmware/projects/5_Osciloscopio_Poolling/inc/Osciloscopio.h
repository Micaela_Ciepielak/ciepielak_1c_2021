/*! @mainpage 5_Osciloscopio_Poolling
 *
 * \section Descripcion General
 *
 *
 *	Aplicación que  convierte una señal analogica, como podría ser una señal de tensión regulada con un potenciometro, a digital (por poolling)y se
 *	la transmite a un graficador de puerto serie de la PC.
 *	Se utiliza una frecuencia de muestreo de 500Hz.
 *
 *
 *
 * \section hardConn Coneccion de Hardware
 *
 *	Coneccion de Hardware:
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 		PIN1 	| 	CH1			|
 * | 		+5V	 	| 	+5V			|
 * | 		GND	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Fecha    | Descripcion                                    |
 * |:----------:|:-----------------------------------------------|
 * | 21/05/2021 | Creacion del proyecto			                 |
 * | 04/06/2021 | Creacion de la documentacion	                 |
 * | 			| 	                     						 |
 *
 * @author Micaela Ciepielak
 *
 */
#ifndef _BLINKING_SWITCH_H
#define _BLINKING_SWITCH_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

