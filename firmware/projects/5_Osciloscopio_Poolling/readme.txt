Proyecto: Interfaz de salida numérica

Diseñar y desarrollar un sistema embebido que controle una interfaz de salida LCD de 3 dígitos.
El sistema deberá poder mostrar un número decimal enviado a través de 4 lineas de puerto (D1,D2,D3 Y D4) y 3 líneas de control (Sel_0,Sel_1,Sel_2). 

Tener en cuenta el Poncho LCD Numérico que utiliza el display ITS_E0803.
Requerimientos de firmware:

Nombre del driver: DisplayITS_E0803.c  
Cabecera del driver: DisplayITS_E0803.h
Funciones:
bool ITSE0803Init(gpio_t * pins)
bool ITSE0803DisplayValue(uint16_t valor)
uint16_t ITSE0803ReadValue(void)
bool ITSE0803Deinit(gpio_t * pins)




_______________________________________________________________________
Mostrar distancia medida utilizando los leds de la siguiente manera:

Si la distancia está entre 0 y 10 cm, encender el LED_RGB_B (Azul).
Si la distancia está entre 10 y 20 cm, encender el LED_RGB_B (Azul) y LED_1.
Si la distancia está entre 20 y 30 cm, encender el LED_RGB_B (Azul), LED_1 y LED_2
Si la distancia es mayor a 30 cm, encender el LED_RGB_B (Azul), LED_1, LED_2 y LED_3.

Mostrar el valor de distancia en cm utilizando el display LCD (si dispone de uno).

______________________________________________________________________
Usar TEC1 para activar y detener la medición.
Usar TEC2 para mantener el resultado (“HOLD”).


