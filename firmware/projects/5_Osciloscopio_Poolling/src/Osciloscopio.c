/*
 * Electrónica Programable
 * FIUNER - 2021
 * Autora: Micaela Ciepielak
 */

/*==================[inclusions]=============================================*/

#include "gpio.h"
#include "analog_io.h"
#include "systemclock.h"
#include "timer.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/


void AnalogicoDigitalPoolling(void) {
	uint16_t valor;
	AnalogInputReadPolling( CH1, &valor);
	UartSendString(SERIAL_PORT_PC, UartItoa(valor, 10));
	UartSendString(SERIAL_PORT_PC, "\r");
}



 timer_config my_timer = { TIMER_A, 2, AnalogicoDigitalPoolling};
 analog_input_config senial_poolling = {CH1, AINPUTS_SINGLE_READ, NULL};
 serial_config serie = { SERIAL_PORT_PC, 115200, NULL };

void SisInit(void) {

	SystemClockInit();
	AnalogInputInit(&senial_poolling);
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	UartInit(&serie);

}



int main(void) {

	SisInit();




	return 0;
}

/*==================[end of file]============================================*/

