/*! @mainpage 2_blinking_switch
 *
 * \section Descripcion General
 *
 *	Mateniendo la tecla 1 y 2 apretadas blinquea el led rojo.
 *	Mateniendo la tecla 1 y 3 apretadas blinquea el led verde.
 *	Mateniendo la tecla 1 y 4 apretadas blinquea el led azul.
 *  La tecla 2 blinquea el Led 1, la tecla 3 blinquea el Led 2 y la tecla 4 blinquea el Led 3
 *
 *
 * \section hardConn Coneccion de Hardware
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	----	 	| 	--------	|
 *
 * @section changelog Changelog
 *
 * |   Fecha    | Descripcion                                    |
 * |:----------:|:-----------------------------------------------|
 * | 09/04/2021	| Creacion del proyecto    						 |
 * | 14/05/2021 | Creacion de la documentacion	                 |
 *
 * @author Micaela Ciepielak
 *
 */

#ifndef _BLINKING_SWITCH_H
#define _BLINKING_SWITCH_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

