/*
 * Electrónica Programable
 * FIUNER - 2021
 * Autor/es: Micaela Ciepielak
 */

/*==================[inclusions]=============================================*/
#include "blinking_switch.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/
#define COUNT_DELAY 3000000
/*==================[internal data definition]===============================*/

void Delay(void) {
	uint32_t i;

	for (i = COUNT_DELAY; i != 0; i--) {
		asm ("nop");
	}
}

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void) {

	// configuracion
	uint8_t teclas;
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	// bucle infinito
	while (1) {
		teclas = SwitchesRead();

		if (teclas == (SWITCH_1 | SWITCH_2)) {
			LedOn(LED_RGB_R);
			Delay();
			LedOff(LED_RGB_R);
			Delay();
		}
		if (teclas == (SWITCH_1 | SWITCH_3)) {
			LedOn(LED_RGB_G);
			Delay();
			LedOff(LED_RGB_G);
			Delay();
		}
		if (teclas == (SWITCH_1 | SWITCH_4)) {
			LedOn(LED_RGB_B);
			Delay();
			LedOff(LED_RGB_B);
			Delay();
		}

		switch (teclas) {
		case SWITCH_2:
			LedOn(LED_1);
			Delay();
			LedOff(LED_1);
			Delay();
			break;
		case SWITCH_3:
			LedOn(LED_2);
			Delay();
			LedOff(LED_2);
			Delay();
			break;
		case SWITCH_4:
			LedOn(LED_3);
			Delay();
			LedOff(LED_3);
			Delay();
			break;
		}
	}

}

/*==================[end of file]============================================*/

