/*
 * Electrónica Programable
 * FIUNER - 2021
 * Autora: Micaela Ciepielak
 */

/*==================[inclusions]=============================================*/
#include "template.h"       /* <= own header */
#include "gpio.h"
#include "systemclock.h"
#include "hc_sr4.h"
#include "timer.h"
#include "uart.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/

#define cantidad_aberturas 20
#define diametro_cm 32

/*==================[internal data definition]===============================*/

double cm_por_abertura = diametro_cm / cantidad_aberturas;
uint16_t contador = 0;
bool OnOff = false;

/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/

double acumulador = 0;

/*==================[external functions definition]==========================*/


void MedirDistancia(){

	if(HcSr04ReadDistanceCentimeters() > 0){ /*suponiendo que el sensor mide 0 cuando no pasa por una ranura, se genera el pulso*/
		contador++;
		acumulador =+ cm_por_abertura * contador;
	}

}

void Encendido() {

	OnOff =true;

}

void Apagado() {

	OnOff = false;

}

void Reseteo(){
	contador = 0;
}


void MostrarDistancia() {

if(OnOff = true){
	/* muestro la distancia */
	UartSendString(SERIAL_PORT_PC, UartItoa(acumulador, 10));
	UartSendString(SERIAL_PORT_PC, "cm\r\n");

}
else{
	/*muestro cero*/
	UartSendString(SERIAL_PORT_PC, "0 cm\r\n");
}

}

timer_config my_timer = { TIMER_A, 1000, &MostrarDistancia };
serial_config serial = { SERIAL_PORT_PC, 115200, NO_INT };

void SisInit(void) {

	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);
	SystemClockInit();

	SwitchesInit();

	SwitchActivInt(SWITCH_1, Encendido);
	SwitchActivInt(SWITCH_2, Apagado);
	SwitchActivInt(SWITCH_3, Reseteo);

	UartInit(&serial);

	TimerInit(&my_timer);
	TimerStart(TIMER_A);
}


int main(void){

	SisInit();

    while(1){

	}
    
	return 0;
}

/*==================[end of file]============================================*/

