/*! @mainpage Parcial_1
 *
 * \section Descripcion General
 *
 *	Aplicacion que permite a un odometro, que es un medidor de distancia que cuenta la cantidad de pulsos generados por un disco perforado
 *	solidario a una rueda. Se diseña para un disco que posee 20 aberturas y la rueda 32cm de diámetro.
 *	Mientras está en funcionamiento, la aplicacion acumulan la distancia recorrida,
 *	e informarla a través de un puerto cada un segundo, con el formato “XX cm\r\n”.
 *	Para el control del sistema se deben usar las teclas:
 *	Tecla 1: Encendido.
 *	Tecla 2: Apagado.
 *	Tecla 3: Reseteo de cuenta.
 *
 * \section hardConn Coneccion de Hardware
 *
 * 	Coneccion de Hardware del sensor del odometro:
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 		ECHO 	| 	T_FILE2		|
 * | 		TRIGGER	| 	T_FILE3		|
 * | 		+5V	 	| 	+5V			|
 * | 		GND	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Fecha    | Descripcion                                    |
 * |:----------:|:-----------------------------------------------|
 * | 09/06/2021 | Creacion del proyecto			                 |
 * | 			| 	                     						 |
 *
 * @author Micaela Ciepielak
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

