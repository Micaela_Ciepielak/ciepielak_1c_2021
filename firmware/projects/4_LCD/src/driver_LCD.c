/*
 * Electrónica Programable
 * FIUNER - 2021
 * Autora: Micaela Ciepielak
 */

/*==================[inclusions]=============================================*/
#include "../../4_LCD/inc/driver_LCD.h"       /* <= own header */

#include "../../../modules/lpc4337_m4/drivers_devices/inc/DisplayITS_E0803.h"
#include "gpio.h"
#include "hc_sr4.h"
#include "led.h"
#include "systemclock.h"
#include "switch.h"
#include "delay.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/
#define MASC 0x01

gpio_t pins[7] = { GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1,
		GPIO_3, GPIO_5 };

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
void PrenderLedsPorDistancia(uint16_t dist) {
	if (dist < 10) {
		LedOn(LED_RGB_B);
		LedOff(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);
	}
	if (dist < 20 && dist > 10) {
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);
	}
	if (dist < 30 && dist > 20) {
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOn(LED_2);
		LedOff(LED_3);
	}
	if (dist > 30) {
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOn(LED_2);
		LedOn(LED_3);

	}
}
int main(void) {

	ITSE0803Init(pins);
	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);
	LedsInit();
	SystemClockInit();
	SwitchesInit();
	static unsigned char estado1 = 0;
	static unsigned char estado2 = 0;
	uint8_t teclas;
	uint16_t distancia;

	while (1) {

		teclas = SwitchesRead();

		switch (teclas) {
		case SWITCH_1:		//Usar TEC1 para activar y detener la medición.

			estado1 = estado1 + 1;
			DelayUs(600);
			break;

		case SWITCH_2:		//Usar TEC2 para mantener el resultado (“HOLD”).
			estado2 = estado2 + 1;
			DelayUs(600);
			break;

		}

		if (estado1 & MASC) {
			distancia = HcSr04ReadDistanceCentimeters();

			if (estado2 & MASC) {
			} else {
				ITSE0803DisplayValue(distancia);
				PrenderLedsPorDistancia(distancia);
			}
		}

		else {
			ITSE0803DisplayValue(0);

		}
		DelayUs(300);

	}
}

/*==================[end of file]============================================*/

