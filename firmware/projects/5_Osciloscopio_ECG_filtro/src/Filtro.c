/*
 * Electrónica Programable
 * FIUNER - 2021
 * Autora: Micaela Ciepielak
 */

/*==================[inclusions]=============================================*/

#include "gpio.h"
#include "analog_io.h"
#include "systemclock.h"
#include "timer.h"
#include "uart.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/

#define BUFFER_SIZE 231
#define DELTA_T 0.002
#define PI 3.1415



/*==================[internal data definition]===============================*/

const char ecg[BUFFER_SIZE]={
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
};

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
	uint8_t contador = 0;
	uint16_t valor;
	uint16_t salida_filtrada_anterior = 0;
	uint16_t salida_filtrada;
	bool estado_filtro = false;

	float fc = 10;

float alfa(){
		float alfa;
		float RC;
		RC = 1 / (2*PI*fc);
		alfa = DELTA_T / (DELTA_T + RC );
		return alfa;
}

void IncrementarFc(){
	fc = fc + 2;
}

void DecrementarFc(){
	fc = fc - 2;
}


void ActivarFiltro(){

	estado_filtro = true;

}

void DesactivarFiltro(){
	estado_filtro = false;
}

void LeerECG(){

	if(contador < BUFFER_SIZE){
		AnalogOutputWrite(ecg[contador]);
				contador++;
	}
	if(contador == BUFFER_SIZE){
		contador=0;
	}
}

void Filtro(){
	salida_filtrada= salida_filtrada_anterior+alfa()*(valor-salida_filtrada_anterior);
	salida_filtrada_anterior = salida_filtrada;
	UartSendString(SERIAL_PORT_PC, UartItoa(salida_filtrada, 10));		//convierto el valor a string para poder mostrarlo
	UartSendString(SERIAL_PORT_PC, "\r"); 	//necesita el fin de linea para tomarlo
}

void AnalogicoDigital(void) {

	SwitchesRead();
	AnalogInputRead(CH1, &valor);    //leo el valor del canal

	SwitchActivInt(SWITCH_1 , DesactivarFiltro);

	SwitchActivInt(SWITCH_2 , ActivarFiltro);

	SwitchActivInt(SWITCH_3 , IncrementarFc);

	SwitchActivInt(SWITCH_4 , DecrementarFc);

	if(!estado_filtro){
	UartSendString(SERIAL_PORT_PC, UartItoa(valor, 10));		//convierto el valor a string para poder mostrarlo
	UartSendString(SERIAL_PORT_PC, "\r"); 	//necesita el fin de linea para tomarlo
	}
	else{
		Filtro();
	}
}


void InicializacionInterrupcion(){

	AnalogStartConvertion();		//comienza la conversion analogica digital

}


timer_config my_timer = { TIMER_A, 2, InicializacionInterrupcion};
timer_config my_timer_ecg = { TIMER_B, 4, LeerECG};
analog_input_config senial_interrupcion= {CH1, AINPUTS_SINGLE_READ, &AnalogicoDigital};
serial_config serie = { SERIAL_PORT_PC, 115200, NULL };

void SisInit(void) {
	SystemClockInit();
	SwitchesInit();
	TimerInit(&my_timer);
	TimerInit(&my_timer_ecg);

	AnalogInputInit(&senial_interrupcion);
	AnalogOutputInit();

	UartInit(&serie);

	TimerStart(TIMER_A);
	TimerStart(TIMER_B);

}





int main(void) {

	SisInit();




	return 0;
}

/*==================[end of file]============================================*/

