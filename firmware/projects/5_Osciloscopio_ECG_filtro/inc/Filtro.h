/*! @mainpage 5_Osciloscopio_filtro
 *
 * \section Descripcion General
 *
 *
 *	Aplicación que utiliza una señal digital de ECG y se la convierte en una señal analógica. Luego se la visualiza utilizando un
 *	osciloscopio ya implementado.
 *	Posee un filtro digital pasa-bajo aplicado sobre la señal de ECG:
 *		- La tecla 1 para activar el uso del filtro.
 *		- La tecla 2 para desactivar el uso del filtro.
 *		- La tecla 3 para bajar la frecuencia de corte.
 *		- La tecla 4 para subir la frecuencia de corte.
 *
 *
 * 	Aclaración: como se tiene una señal de ECG precargada se debe puentear, conectando la salida del DAC a la entrada CH1 del AD.
 *
 *
 * \section hardConn Coneccion de Hardware
 *
 *
 *
 *	Coneccion de Hardware:
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 		PIN1 	| 	CH1			|
 * | 		PIN1 	| 	DAC			|
 * | 		+5V	 	| 	+5V			|
 * | 		GND	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Fecha    | Descripcion                                    |
 * |:----------:|:-----------------------------------------------|
 * | 28/05/2021 | Creacion del proyecto			                 |
 * | 04/06/2021 | Creacion de la documentacion	                 |
 * | 			| 	                     						 |
 *
 * @author Micaela Ciepielak
 *
 */
#ifndef _BLINKING_SWITCH_H
#define _BLINKING_SWITCH_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

