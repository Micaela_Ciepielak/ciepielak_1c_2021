/*
 * Electrónica Programable
 * FIUNER - 2021
 * Autora: Micaela Ciepielak
 */

/*==================[inclusions]=============================================*/
#include "../../Afinador_Guitarra/inc/afinador_guitarra.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "analog_io.h"
#include "switch.h"
#include "timer.h"
#include "fpu_init.h"
#include "delay.h"
#include "../inc/DisplayITS_E0803.h"

#define ARM_MATH_CM4
#define __FPU_PRESENT 1

#include "arm_math.h"
#include "arm_const_structs.h"

#define IFFT_FLAG 0
#define DO_BIT_REVERSE 1
#define ANCHO_VENTANA 128
#define SAMPLE_FREC_US (1000000/800)
#define cantidad_cuerdas 6

gpio_t pins[7] = { GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1, GPIO_3, GPIO_5 };
const uint16_t frecuencia_cuerda[cantidad_cuerdas] = { 329.63, 246.94, 196.00, 146.83, 110.00, 82.40 }; /*Hz*/

bool On_Off = true;
bool nueva_ventana = false;

uint8_t numero_cuerda = 0;
float32_t senial[2 * ANCHO_VENTANA];
uint8_t i, frecuencia_actual;

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

void OnOff() {
	On_Off = !On_Off;
	ITSE0803DisplayValue(0);
	DelayUs(50);
}

void Reinicio() {
	numero_cuerda = 0;
//	DelayUs(50);
}

void SiguienteCuerda() {
	bool estado = true;

	if (estado) {
		if (numero_cuerda < (cantidad_cuerdas - 1)) {
			numero_cuerda = numero_cuerda + 1;
			estado = false;
		} else {
			numero_cuerda = 0;
			estado = false;
		}
	}
	DelayUs(50);
}

void AnteriorCuerda() {

	if (numero_cuerda == 0) {
		numero_cuerda = cantidad_cuerdas - 1;
	} else {
		numero_cuerda = numero_cuerda - 1;
	}
	DelayUs(50);
}

void ConversorAnalogicoDigital() {
	static uint8_t i = 0;
	uint16_t audio;

	AnalogInputRead(CH1, &audio);

	if (i < ANCHO_VENTANA) {
		senial[i * 2] = audio * (3.3 / 1024); /* Parte real */
		senial[i * 2 + 1] = 0; /* Parte imaginaria */
		i++;
	} else {
		i = 0;
		nueva_ventana = true;
	}
}

void InicializacionCoversion() {
	AnalogStartConvertion();
}

void Afinador() {
	float aux_2 = 8;
	uint8_t n=numero_cuerda+1;
	 aux_2 = (800/ANCHO_VENTANA);
	float error_frecuencia = (1 * aux_2);
	ITSE0803DisplayValue(n);

	if ((aux_2 * frecuencia_actual)
			< (frecuencia_cuerda[numero_cuerda] - error_frecuencia)) {
		LedOff(LED_2);
		LedOff(LED_3);

		LedOn(LED_1);
	}
	if ((aux_2 * frecuencia_actual)
			> (frecuencia_cuerda[numero_cuerda] + error_frecuencia)) { /*Si mi frecuencia es más aguda -> aflojo */
		LedOff(LED_1);
		LedOff(LED_2);

		LedOn(LED_3);
	}
	if (((aux_2 * frecuencia_actual)
			< (frecuencia_cuerda[numero_cuerda] + error_frecuencia))
			& ((aux_2 * frecuencia_actual)
					> (frecuencia_cuerda[numero_cuerda] - error_frecuencia))) {
		LedOff(LED_1);
		LedOff(LED_3);

		LedOn(LED_2);
	}
	if (frecuencia_actual == 0) {
		LedsOffAll();
	}
}

void Procesador() {
	if (On_Off) {
		if (nueva_ventana) {
			float32_t fft[ANCHO_VENTANA];
			float32_t potencia_max = 0;

			arm_cfft_f32(&arm_cfft_sR_f32_len128, senial, IFFT_FLAG,
					DO_BIT_REVERSE);
			arm_cmplx_mag_f32(senial, fft, ANCHO_VENTANA);
			nueva_ventana = false;

			/* Obtencion del valor maximo del espectro de frecuencias */
			for (i = 2; i < ANCHO_VENTANA; i++) {
				if (fft[i] > potencia_max) {
					potencia_max = fft[i];
					frecuencia_actual = i;

				}
			}

			potencia_max = 0;
		}
		Afinador();
	} else {
		LedOn(LED_1);
		LedOn(LED_2);
		LedOn(LED_3);
	}

}
/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
analog_input_config AD_conversor = { CH1, AINPUTS_SINGLE_READ, &ConversorAnalogicoDigital };

timer_config timer_init_convertion = { TIMER_C, SAMPLE_FREC_US,	InicializacionCoversion };

void InicializadorSistema(void) {

	SystemClockInit();
	LedsInit();
	fpuInit();
	SwitchesInit();
	AnalogInputInit(&AD_conversor);
	ITSE0803Init(pins);

	SwitchActivInt(SWITCH_1, OnOff);
	SwitchActivInt(SWITCH_2, Reinicio);
	SwitchActivInt(SWITCH_3, AnteriorCuerda);
	SwitchActivInt(SWITCH_4, SiguienteCuerda);

	TimerInit(&timer_init_convertion);
	TimerStart(TIMER_C);
}

int main(void) {

	InicializadorSistema();

	while (1) {

		Procesador();

	}

	return 0;
}

/*==================[end of file]============================================*/

