/*! @mainpage Afinador
 *
 * \section Descripcion General
 *
 *	Aplicación para afinar una guitarra.
 *	Teniendo en cuenta los tonos de cada cuerda:
 *	1° cuerda, mi = 329,63 Hz
 *	2° cuerda, Si = 246,94 Hz
 *	3° cuerda, Sol = 196,00 Hz
 *	4° cuerda, Re 146,83 Hz
 *	5° cuerda, La 110,00 Hz
 *	6° cuerda, Mi 82,40 Hz
 *
 *	Cuando el tono es el correcto el led 2 se prenderá, cuando necesite ajustarse la cuerda (se necesita más frecuencia) se prenderá el led 1,
 *	mientras que si se necesita aflojar la cuerda (se necesita bajar la frecuencia) se prenderá el led 3.
 *	Tecla 1: On/Off
 *	Tecla 2: Reinicia
 *	Tecla 3: Pasa a la cuerda anterior
 *	Tecla 4: Pasa a la siguiente cuerda
 *
 * \section hardConn Coneccion de Hardware
 *
 * 	Coneccion de Hardware del microfono:
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 		OUT 	| 	CH1			|
 * | 		+5V	 	| 	+5V			|
 * | 		GND	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Fecha    | Descripcion                                    |
 * |:----------:|:-----------------------------------------------|
 * | 11/06/2021 | Creacion del proyecto			                 |
 * | 			| 	                     						 |
 *
 * @author Micaela Ciepielak
 *
 */

#ifndef _BLINKING_H
#define _BLINKING_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

