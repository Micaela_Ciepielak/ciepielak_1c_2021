/*
 * Electrónica Programable
 * FIUNER - 2021
 * Autora: Micaela Ciepielak
 */

/*==================[inclusions]=============================================*/
#include "template.h"       /* <= own header */
#include "gpio.h"
#include "analog_io.h"
#include "systemclock.h"
#include "timer.h"
#include "uart.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/

#define angulo_maximo 180
#define tension_maxima 3.3

/*==================[internal data definition]===============================*/

bool OnOff = false;

/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/

double angulo_max_medido = 0;
double angulo_actual = 0;

/*==================[external functions definition]==========================*/


void Encendido() {

	OnOff =true;

}

void Apagado() {

	OnOff = false;

}

void Reseteo(){
	angulo_actual = 0;
}

void InicializacionInterrupcion(){
	AnalogStartConvertion();
}

void ConversorAnalogicoDigital(void) {
	double tension;
	AnalogInputRead(CH1, &tension);

	angulo_actual = tension * angulo_maximo / tension_maxima;

	if(angulo_max_medido < angulo_actual){
		angulo_max_medido = angulo_actual;
	}
	if(OnOff = true){
		/* muestro el angulo actual y el maximo */
	UartSendString(SERIAL_PORT_PC, UartItoa(angulo_actual, 10));
	UartSendString(SERIAL_PORT_PC, "° ");
	UartSendString(SERIAL_PORT_PC, UartItoa(angulo_max_medido, 10));
	UartSendString(SERIAL_PORT_PC, "° \r\n");
	}
	else{
		/*muestro cero*/
		UartSendString(SERIAL_PORT_PC, "0° \r\n");
	}

}

timer_config timer_init_convertion = { TIMER_A, 2, InicializacionInterrupcion};

serial_config serial = { SERIAL_PORT_PC, 115200, NO_INT };

analog_input_config senial_goniometro= {CH1, AINPUTS_SINGLE_READ, &ConversorAnalogicoDigital};

void SisInit(void) {

	SystemClockInit();
	AnalogInputInit(&senial_goniometro);
	UartInit(&serial);
	SwitchesInit();

	SwitchActivInt(SWITCH_1, Encendido);
	SwitchActivInt(SWITCH_2, Apagado);
	SwitchActivInt(SWITCH_3, Reseteo);

	TimerInit(&timer_init_convertion);
	TimerStart(TIMER_A);
}


int main(void){

	SisInit();

    while(1){

	}
    
	return 0;
}

/*==================[end of file]============================================*/

