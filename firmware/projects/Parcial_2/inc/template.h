/*! @mainpage Parcial_2
 *
 * \section Descripcion General
 *
 *	Aplicacion que permite a un goniómetro digital de rodilla que utiliza un potenciómetro solidario al eje de dos piezas móviles.
 *	Para esto digitaliza la señal del punto medio del potenciómetro	y calcula el ángulo, sabiendo a 0º la tensión de salida es 0V y a 180º es 3.3V.
 *	El sistema además enviar a través de un puerto serie tanto el valor del	ángulo actual como el valor máximo medido hasta el momento
 *	en formato	(“XXº - YYº\r\n” donde XX es el valor de ángulo actual y YY el valor máximo de ángulo medido hasta ese instante).
 *	Para el control del sistema se deben usar las teclas:
 *	Tecla 1: Encendido.
 *	Tecla 2: Apagado.
 *	Tecla 3: Reseteo de cuenta.
 *
 * \section hardConn Coneccion de Hardware
 *
 *	Coneccion de Hardware del potenciometro:
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 		PIN1 	| 	CH1			|
 * | 		+5V 	| 	+5V			|
 * | 		GND	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Fecha    | Descripcion                                    |
 * |:----------:|:-----------------------------------------------|
 * | 09/06/2021 | Creacion del proyecto			                 |
 * | 			| 	                     						 |
 *
 * @author Micaela Ciepielak
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

