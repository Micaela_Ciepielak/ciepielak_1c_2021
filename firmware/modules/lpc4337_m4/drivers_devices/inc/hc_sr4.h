/** @mainpage hc_sr4.h
 *
 * \section genDesc Descripción General
 *
 * Este driver se encarga de recibir y procesar la informacion del periferico
 * de medidor de distancia por ultrasonido con el fin de obtener un dato numerico
 * el cual representa la distancia del sensor al objeto.
 * Tiene como rango de medición de 2 a 400 cm.
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO2		|
 * | 	PIN2	 	| 	GPIO3		|
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 05/05/2021 | Document creation		                         |
 * | 			| 	                     						 |
 *
 * @author Vera Marco
 *
 */
#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/
#include <stdint.h>
#include "bool.h"
#include "gpio.h"
/*==================[external functions declaration]=========================*/


//Comentarios arriba de cada función.
bool HcSr04Init(gpio_t echo, gpio_t trigger);
uint16_t HcSr04ReadDistanceCentimeters(void);
uint16_t HcSr04ReadDistanceInches(void);
bool HcSr04Deinit(gpio_t echo, gpio_t trigger);



/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

