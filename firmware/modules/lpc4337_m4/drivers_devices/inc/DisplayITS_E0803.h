/*! @mainpage DisplayITS_E0803
 *
 * \section Descripcion General
 *
 *	Es un sistema embebido que controle una interfaz de salida LCD de 3 dígitos.
 *
 *
 * \section hardConn Coneccion de Hardware
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 		D1	 	| 	LCD1		|
 * | 		D2	 	| 	LCD2		|
 * |	 	D3	 	| 	LCD3		|
 * |	 	D4	 	| 	LCD4		|
 * |	 	SEL_0	| 	GPIO1		|
 * | 		SEL_0	| 	GPIO3		|
 * |	 	SEL_0	| 	GPIO5		|
 * | 		+5V	 	| 	+5V			|
 * | 		GND	 	| 	GND			|
 *
 * @section changelog Changelog
 *
 * |   Fecha    | Descripcion                                    |
 * |:----------:|:-----------------------------------------------|
 * | 05/05/2020 | Creacion de la documentacion	                 |
 * | 			| 	                     						 |
 *
 * @author Micaela Ciepielak
 *
 */
#ifndef _TEMPLATE_H
#define _TEMPLATE_H

#include "gpio.h"
#include "bool.h"


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/




/*==================[external data declaration]==============================*/

/** @fn bool ITSE0803Init(gpio_t * pins)
 * @brief Permite inicializar los puertos
 * @param[in] pines de 3 lineas de puerto y 4 de control como puntero
 * @return retorna 1
 */
bool ITSE0803Init(gpio_t * pins);

/** @fn ITSE0803DisplayValue(uint16_t valor)
 * @brief Muestra valores en pantalla LCD
 * @param[in] valor de numero decimal que se quiere mostrar
 * @return retorna 1
 */
bool ITSE0803DisplayValue(uint16_t valor);

/** @fn ITSE0803ReadValue(void)
 * @brief Lee el valor de pantalla
 * @param void
 * @return retorna el valor uint16_t de la pantalla
 */
uint16_t ITSE0803ReadValue(void);

/** @fn IITSE0803Deinit(gpio_t * pins)
 * @brief Elimina pines de iniciacion
 * @param[in] pines de 3 lineas de puerto y 4 de control como puntero
 * @return
 */
bool ITSE0803Deinit(gpio_t * pins);


/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

