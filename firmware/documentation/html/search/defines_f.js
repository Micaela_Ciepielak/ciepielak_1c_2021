var searchData=
[
  ['usb_5fconfiguartion_5fdesc_5fsize',['USB_CONFIGUARTION_DESC_SIZE',['../usbd__desc_8h.html#a6ce7e811f8523649e7eaa158dd06b8c6',1,'usbd_desc.h']]],
  ['usb_5fdevice_5fdesc_5fsize',['USB_DEVICE_DESC_SIZE',['../usbd__desc_8h.html#acd04660262562260957b6df8d5e482b3',1,'usbd_desc.h']]],
  ['usb_5fdevice_5fquali_5fsize',['USB_DEVICE_QUALI_SIZE',['../usbd__desc_8h.html#a038630c136094c9f727b62cb81a9a186',1,'usbd_desc.h']]],
  ['usb_5fdfu_5fcan_5fdownload',['USB_DFU_CAN_DOWNLOAD',['../usbd__dfu_8h.html#a97f168713ef0523016079e77d1eafbf7',1,'usbd_dfu.h']]],
  ['usb_5fdfu_5fcan_5fupload',['USB_DFU_CAN_UPLOAD',['../usbd__dfu_8h.html#aca456b139d47198b68b679a0a128fe9a',1,'usbd_dfu.h']]],
  ['usb_5fdfu_5fdescriptor_5fsize',['USB_DFU_DESCRIPTOR_SIZE',['../usbd__dfu_8h.html#a7f36c1f1536dfe16c52c1b5408b66c49',1,'usbd_dfu.h']]],
  ['usb_5fdfu_5fdescriptor_5ftype',['USB_DFU_DESCRIPTOR_TYPE',['../usbd__dfu_8h.html#a4229ddca385f29c1712374579736d962',1,'usbd_dfu.h']]],
  ['usb_5fdfu_5fif_5fnum',['USB_DFU_IF_NUM',['../usbd__dfu_8h.html#a9c6096c21b3d4525edce753d8b8e84b6',1,'usbd_dfu.h']]],
  ['usb_5fdfu_5fmanifest_5ftol',['USB_DFU_MANIFEST_TOL',['../usbd__dfu_8h.html#a86ace3ed8d127c3107426b37cee3c0ea',1,'usbd_dfu.h']]],
  ['usb_5fdfu_5fsubclass',['USB_DFU_SUBCLASS',['../usbd__dfu_8h.html#a518e936748d79f70576e082604203ae7',1,'usbd_dfu.h']]],
  ['usb_5fdfu_5fwill_5fdetach',['USB_DFU_WILL_DETACH',['../usbd__dfu_8h.html#a01f15f0d059477270366799015500a41',1,'usbd_dfu.h']]],
  ['usb_5fendpoint_5fdesc_5fsize',['USB_ENDPOINT_DESC_SIZE',['../usbd__desc_8h.html#a317f2697db907bff3f818a644d231c08',1,'usbd_desc.h']]],
  ['usb_5finterface_5fdesc_5fsize',['USB_INTERFACE_DESC_SIZE',['../usbd__desc_8h.html#ac7fd274cdd131f4c0addec7a139bc9b7',1,'usbd_desc.h']]],
  ['usb_5fother_5fspeed_5fconf_5fsize',['USB_OTHER_SPEED_CONF_SIZE',['../usbd__desc_8h.html#ab7288d5e0e645952c8c156182f3baa11',1,'usbd_desc.h']]],
  ['usb_5freq_5fdfu_5fabort',['USB_REQ_DFU_ABORT',['../usbd__dfu_8h.html#ac84973c4288582edbda8785e87d0d2a2',1,'usbd_dfu.h']]],
  ['usb_5freq_5fdfu_5fclrstatus',['USB_REQ_DFU_CLRSTATUS',['../usbd__dfu_8h.html#a40457acfe7124e402ec9c164710d642c',1,'usbd_dfu.h']]],
  ['usb_5freq_5fdfu_5fdetach',['USB_REQ_DFU_DETACH',['../usbd__dfu_8h.html#adf05df70e99a9134659d6d65f6ebe405',1,'usbd_dfu.h']]],
  ['usb_5freq_5fdfu_5fdnload',['USB_REQ_DFU_DNLOAD',['../usbd__dfu_8h.html#af88b1add2b88c791f2a9c0c48294e38a',1,'usbd_dfu.h']]],
  ['usb_5freq_5fdfu_5fgetstate',['USB_REQ_DFU_GETSTATE',['../usbd__dfu_8h.html#a079e823748fe9b404c91a6da34896a8d',1,'usbd_dfu.h']]],
  ['usb_5freq_5fdfu_5fgetstatus',['USB_REQ_DFU_GETSTATUS',['../usbd__dfu_8h.html#a8a56dbb7249fd8d0707d4447de5e28b4',1,'usbd_dfu.h']]],
  ['usb_5freq_5fdfu_5fupload',['USB_REQ_DFU_UPLOAD',['../usbd__dfu_8h.html#ae0d0d3062eab3210449d0fb49fa9f837',1,'usbd_dfu.h']]],
  ['usbd_5fapi',['USBD_API',['../usbd__rom__api_8h.html#a917f89ab1130705af952ee4252032497',1,'usbd_rom_api.h']]]
];
