var searchData=
[
  ['g_5fpusbapi',['g_pUsbApi',['../usbd__rom__api_8h.html#aa694a4a7a106972c3e4bd17e03ab8f3b',1,'usbd_rom_api.h']]],
  ['gdr',['GDR',['../struct_l_p_c___a_d_c___t.html#a2272ff4c98c72be44cbb1f47f4bc3100',1,'LPC_ADC_T']]],
  ['ge',['GE',['../union_a_p_s_r___type.html#aa91800ec6e90e457c7a1acd1f2e17099',1,'APSR_Type::GE()'],['../unionx_p_s_r___type.html#aa91800ec6e90e457c7a1acd1f2e17099',1,'xPSR_Type::GE()']]],
  ['genrand',['GenRand',['../struct_o_t_p___a_p_i___t.html#a0550aafd5d61c1953774c48ee329d480',1,'OTP_API_T']]],
  ['getcommfeature',['GetCommFeature',['../struct_u_s_b_d___c_d_c___i_n_i_t___p_a_r_a_m.html#a49194845cd8fe069b674756634ddc8a3',1,'USBD_CDC_INIT_PARAM']]],
  ['getencpsresp',['GetEncpsResp',['../struct_u_s_b_d___c_d_c___i_n_i_t___p_a_r_a_m.html#aa59a8e34de6912875959b270de09160b',1,'USBD_CDC_INIT_PARAM']]],
  ['getmemsize',['GetMemSize',['../struct_u_s_b_d___c_d_c___a_p_i.html#a66487e730c2a8648196f23f475067cdf',1,'USBD_CDC_API::GetMemSize()'],['../struct_u_s_b_d___d_f_u___a_p_i.html#a048df985da6f33c0a5c911b50c9f8e75',1,'USBD_DFU_API::GetMemSize()'],['../struct_u_s_b_d___h_i_d___a_p_i.html#a8f7a207918421d50c92bd23e37f494fe',1,'USBD_HID_API::GetMemSize()'],['../struct_u_s_b_d___h_w___a_p_i.html#a6b61fb5a82eab9e8f89aa906877f5dff',1,'USBD_HW_API::GetMemSize()'],['../struct_u_s_b_d___m_s_c___a_p_i.html#ab65dba1f2c48e61a83740793b2331ae3',1,'USBD_MSC_API::GetMemSize()']]],
  ['gl',['Gl',['../struct_l_c_d___p_a_l_e_t_t_e___e_n_t_r_y___t.html#ac2ef5652c3036f4e2e6726827358ce36',1,'LCD_PALETTE_ENTRY_T']]],
  ['gpio',['GPIO',['../struct_l_p_c___s_d_m_m_c___t.html#aa172d5c5fff46adafbd37892a1206432',1,'LPC_SDMMC_T::GPIO()'],['../group___g_p_i_o.html#gafeef070684da598ae03e7acc19344b51',1,'gpio():&#160;gpio.c']]],
  ['gpiopin',['gpioPin',['../structdigital_i_o.html#aac0719041a981c2239101770e56567d6',1,'digitalIO']]],
  ['gpioport',['gpioPort',['../structdigital_i_o.html#af3118cdc8c5891b2284ddf62a43b12c6',1,'digitalIO']]],
  ['gu',['Gu',['../struct_l_c_d___p_a_l_e_t_t_e___e_n_t_r_y___t.html#a5c500dbd0da9c6dee01113a75681c8e6',1,'LCD_PALETTE_ENTRY_T']]]
];
