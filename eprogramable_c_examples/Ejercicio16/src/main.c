/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>


/*==================[macros and definitions]=================================*/


uint32_t Var = 0x1020304;
uint8_t a;
uint8_t b;
uint8_t c;
uint8_t d;


uint8_t Aux1 ;
uint32_t Aux2 ;

union bits{
	struct{
		uint8_t byte1;
		uint8_t byte2;
		uint8_t byte3;
		uint8_t byte4;
	}cada_byte;
	uint32_t  todos_los_bytes;
} B;



/*==================[internal functions declaration]=========================*/


int main(void)
{
	// a)
	Aux1 = (Var >> 24);
	printf("Inciso a): \n");
	a = Aux1;

	Aux2 = (Var << 8);
	Aux1 = (Aux2 >>24);

	b = Aux1;

	Aux2 = (Var <<16);
	Aux1 = (Aux2 >> 24);
	c = Aux1;

	Aux2 = (Var << 24);
	Aux1 = (Aux2 >> 24);
	d = Aux1;


	printf("Variable: %d \n", Var);
	printf("a: %d \n", a);

	printf("b: %d \n", b);
	printf("c: %d \n", c);
	printf("d: %d \n \n", d);


	// b)
	B.todos_los_bytes = Var;
	printf("Inciso b) \na: %d \n", B.cada_byte.byte4);
	printf("b: %d \n", B.cada_byte.byte3  );
	printf("c: %d \n", B.cada_byte.byte2);
	printf("d: %d \n", B.cada_byte.byte1);

	return 0;

}

/*==================[end of file]============================================*/

