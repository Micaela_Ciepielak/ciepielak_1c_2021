
Ejercicio 16: 
a) Declare una variable sin signo de 32 bits y cargue el valor 0x01020304. 
Declare cuatro variables sin signo de 8 bits y, utilizando máscaras, rotaciones y truncamiento,
 cargue cada uno de los bytes de la variable de 32 bits.
 
b) Realice el mismo ejercicio, utilizando la definición de una “union”.


Para ejecutar, abrir una consola ubicarse en el directorio Ejercicio16/ y ejecutar ./Ejercicio16.exe
