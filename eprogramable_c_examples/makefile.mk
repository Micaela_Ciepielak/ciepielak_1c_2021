########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
####Ejemplo 1: Hola Mundo
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = 1_hola_mundo
#NOMBRE_EJECUTABLE = hola_mundo.exe

####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

#PROYECTO_ACTIVO = 5_menu
#NOMBRE_EJECUTABLE = menu.exe

####Ejercicio 7 guía1
#PROYECTO_ACTIVO = Ejercicio7
#NOMBRE_EJECUTABLE = Ejercicio7.exe

####Ejercicio 9 guía1
#PROYECTO_ACTIVO = Ejercicio9
#NOMBRE_EJECUTABLE = Ejercicio9.exe

####Ejercicio 12 guía1
#PROYECTO_ACTIVO = Ejercicio12
#NOMBRE_EJECUTABLE = Ejercicio12.exe

####Ejercicio 14 guía1
#PROYECTO_ACTIVO = Ejercicio14
#NOMBRE_EJECUTABLE = Ejercicio14.exe

####Ejercicio 16 guía1
#PROYECTO_ACTIVO = Ejercicio16
#NOMBRE_EJECUTABLE = Ejercicio16.exe

####Ejercicio 17 guía1
#PROYECTO_ACTIVO = Ejercicio17
#NOMBRE_EJECUTABLE = Ejercicio17.exe

####Ejercicio Integrador A guía1
#PROYECTO_ACTIVO = IntegradorA
#NOMBRE_EJECUTABLE = IntegradorA.exe

####Ejercicio Integrador C guía1
#PROYECTO_ACTIVO = IntegradorC
#NOMBRE_EJECUTABLE = IntegradorC.exe

####Ejercicio Integrador D guía1
PROYECTO_ACTIVO = IntegradorD
NOMBRE_EJECUTABLE = IntegradorD.exe

#PROYECTO_ACTIVO = Ej_c_d_e
#NOMBRE_EJECUTABLE = c_d_e.exe
