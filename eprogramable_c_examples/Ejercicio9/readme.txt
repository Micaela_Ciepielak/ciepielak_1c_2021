
Ejercicio 9: Sobre una constante de 32 bits previamente declarada, verifique si el bit 4 es 0. 
Si es 0, cargue una variable “A” previamente declarada en 0, si es 1, cargue “A” con 0xaa.
 Para la resolución de la lógica, siga el diagrama de flujo siguiente:


Para ejecutar, abrir una consola ubicarse en el directorio Ejercicio7/ y ejecutar ./Ejercicio7.exe
