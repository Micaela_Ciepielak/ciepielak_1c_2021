Integrador D:
Escribir una función que reciba como parámetro un dígito BCD y un vector de
 estructuras del tipo  gpioConf_t.

typedef struct
{
	uint8_t port;				/*!< GPIO port number */
	uint8_t pin;				/*!< GPIO pin number */
	uint8_t dir;				/*!< GPIO direction ‘0’ IN;  ‘1’ OUT */
} gpioConf_t;

Defina un vector que mapee los bits de la siguiente manera:
b0 -> puerto 1.4
b1 -> puerto 1.5
b2 -> puerto 1.6
b3 -> puerto 2.14



Para ejecutar, abrir una consola ubicarse en el directorio IntegradorD/ y ejecutar ./IntegradorD.exe
