/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
/*==================[macros and definitions]=================================*/

uint8_t masc = 1;


typedef struct
{	uint8_t port;				/*!< GPIO port number */
	uint8_t pin;				/*!< GPIO pin number */
	uint8_t dir;				/*!< GPIO direction ‘0’ IN;  ‘1’ OUT */
} gpioConf_t;


/*==================[internal functions declaration]=========================*/

void graficarBCD( uint8_t BCD, gpioConf_t *datos);

void graficarBCD( uint8_t BCD, gpioConf_t *datos){

	uint8_t i;
	for (i = 0; i < 4; i++){
	datos[i].dir = (BCD >> i )& masc;
	// printf ("%d ", b[i] );
	}

	for (i = 0; i < 4; i++){
	switch (i){
	case 0:{
		for(i; i<4;i++){
		printf("%d ", datos[i].dir);
		}
		if (datos[i].dir == 0){
			printf("b0: \n	Puerto %d.%d - Estado: IN \n", datos[i].port, datos[i].pin );
		break;}
		else{
			printf("b0: \n	Puerto %d.%d - Estado: OUT \n", datos[i].port, datos[i].pin );
		break;}

	}
	case 1: {
		if (datos[i].dir == 0){
			printf("b1: \n	Puerto %d.%d - Estado: IN \n", datos[i].port, datos[i].pin );
			break;}
		else{
			printf("b1: \n	Puerto %d.%d - Estado: OUT \n", datos[i].port, datos[i].pin );
		break;}
	}
	case 2: {
		if (datos[i].dir == 0){
			printf("b2: \n	Puerto %d.%d - Estado: IN \n", datos[i].port, datos[i].pin );
		break;}
			else
			printf("b2: \n	Puerto %d.%d - Estado: OUT \n", datos[i].port, datos[i].pin );
		break;
	}
	case 3: {
		if (datos[i].dir == 0)
			printf("b3: \n	Puerto %d.%d - Estado: IN \n", datos[i].port, datos[i].pin );
		else
			printf("b3: \n	Puerto %d.%d - Estado: OUT \n", datos[i].port, datos[i].pin );
		break;
	}
	}
	}



}

int main(void)
{
	gpioConf_t bit[4] ;


	bit[0].port = 1;
	bit[0].pin = 4;
	bit[0].dir = 1;

	bit[1].port = 1;
	bit[1].pin = 5;
	bit[1].dir = 1;

	bit[2].port = 1;
	bit[2].pin = 6;
	bit[2].dir = 1;

	bit[3].port = 2;
	bit[3].pin = 14;
	bit[3].dir = 1;
	uint8_t i=0;


	graficarBCD(9, bit);


//	b0 -> puerto 1.4
//	b1 -> puerto 1.5
//	b2 -> puerto 1.6
//	b3 -> puerto 2.14


	return 0;
}

/*==================[end of file]============================================*/

