
Ejercicio 12: Declare un puntero a un entero con signo de 16 bits y 
cargue inicialmente el valor -1. Luego, mediante máscaras, coloque un 0 en el bit 4.

Para ejecutar, abrir una consola ubicarse en el directorio Ejercicio12/ y ejecutar ./Ejercicio12.exe
