/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

//Declare una estructura “alumno”, con los campos “nombre” de 12 caracteres,
//“apellido” de 20 caracteres y edad.
//Defina una variable con esa estructura y cargue los campos con sus propios datos.
//Defina un puntero a esa estructura y cargue los campos con los datos de su compañero
//(usando acceso por punteros).



/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>


/*==================[macros and definitions]=================================*/

struct Alumno {
char Nombre[12];
char Apellido[20];
uint8_t Edad;

} ;

struct Alumno Alum;
struct Alumno Compa;
struct Alumno *PtrCompa = &Compa;


/*==================[internal functions declaration]=========================*/

int main(void)
{
   strcpy(Alum.Nombre,"Micaela");
   strcpy (Alum.Apellido, "Ciepielak");
   Alum.Edad = 20;

   printf("Nombre: %s \n", Alum.Nombre);
   printf("Apellido: %s \n", Alum.Apellido);
   printf("Edad: %d \n \n", Alum.Edad);

   strcpy( Compa.Nombre,"Juan");
   strcpy (Compa.Apellido, "Perez");
   Compa.Edad = 25;

   printf("Nombre: %s \n", PtrCompa->Nombre);
   printf("Apellido: %s \n", PtrCompa->Apellido);
   printf("Edad: %d \n", PtrCompa->Edad);




	return 0;
}

/*==================[end of file]============================================*/

