
Ejercicio 14: Declare una estructura “alumno”, con los campos “nombre” de 12 caracteres, 
“apellido” de 20 caracteres y edad. 
Defina una variable con esa estructura y cargue los campos con sus propios datos.
Defina un puntero a esa estructura y cargue los campos con los datos de su compañero 
(usando acceso por punteros).


Para ejecutar, abrir una consola ubicarse en el directorio Ejercicio14/ y ejecutar ./Ejercicio14.exe
