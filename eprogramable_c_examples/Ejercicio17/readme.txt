Ejercicio 17:
Realice un programa que calcule el promedio de los 15 números listados abajo, para ello, 
primero realice un diagrama de flujo similar al presentado en el ejercicio 9. 
(Puede utilizar la aplicación Draw.io). Para la implementación, utilice el menor tamaño de
 datos posible:

	{234
	123
	111
	101
	32
	116
	211
	24
	214
	100
	124
	222
	1
	129
	9}


Para ejecutar, abrir una consola ubicarse en el directorio Ejercicio17/ y ejecutar ./Ejercicio17.exe
