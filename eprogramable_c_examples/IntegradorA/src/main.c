/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>


/*==================[macros and definitions]=================================*/

typedef struct
{
	uint8_t n_led;      //indica el número de led a controlar
	uint8_t n_ciclos;   //indica la cantidad de ciclos de encendido/apagado
	uint8_t periodo;    //indica el tiempo de cada ciclo
	uint8_t mode;       //ON = 1, OFF = 2, TOGGLE = 3
} leds;

leds my_leds;
/*==================[internal functions declaration]=========================*/

void LEDS(leds * leds_ptr);

void LEDS(leds * leds_ptr){


	switch(leds_ptr->mode){

	case 1: {
		switch(leds_ptr->n_led){
		case 1: {
			printf("Se enciende led 1 \n");
			break;
		}
		case 2:{
			printf("Se enciende led 2 \n");
			break;
		}
		case 3:{
			printf("Se enciende led 3 \n");
			break;

		}
		}
	break;}

	case 2: {
		switch(leds_ptr->n_led){
				case 1: {
					printf("Se apaga led 1 \n");
					break;
				}
				case 2:{
					printf("Se apaga led 2 \n");
					break;
				}
				case 3:{
					printf("Se apaga led 3 \n");
					break;

				}
				}


	break;}

	case 3: {
		uint8_t i = 0;

		while (i < leds_ptr->n_ciclos ){

			switch(leds_ptr->n_led){
			case 1: {

				printf("Se enciende led 1 \n");
				break;
			}
			case 2:{
				printf("Se enciende led 2 \n");
				break;
			}
			case 3:{
				printf("Se enciende led 3 \n");
				break;
			}
			default: break;
		}
			i++;
			uint8_t j;
			for(j =0; j < leds_ptr->periodo; j++){
			printf("Retardo %d \n", j+1);}

		}

	break;}

	default: break;

	}


}




int main(void)
{
	my_leds.mode = 3;
	my_leds.n_led = 2;
	my_leds.n_ciclos = 2;
	my_leds.periodo = 8;

	LEDS(&my_leds);

	return 0;
}

/*==================[end of file]============================================*/

