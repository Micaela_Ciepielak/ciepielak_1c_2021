A) Realice un función que reciba un puntero a una estructura LED como la que se muestra a 
continuación: 

struct leds
{
	uint8_t n_led;        indica el número de led a controlar
	uint8_t n_ciclos;   indica la cantidad de ciclos de encendido/apagado
	uint8_t periodo;    indica el tiempo de cada ciclo
	uint8_t mode;       ON, OFF, TOGGLE
} my_leds; 


Para ejecutar, abrir una consola ubicarse en el directorio IntegradorA/ y ejecutar ./IntegradorA.exe
