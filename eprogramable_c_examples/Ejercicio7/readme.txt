
Ejercicio 7: 
Sobre una variable de 32 bits sin signo previamente declarada y de valor desconocido, asegúrese de colocar el bit 3 a 1 y los bits 13 y 14 a 0 mediante máscaras y el operador <<.


Para ejecutar, abrir una consola ubicarse en el directorio Ejercicio7/ y ejecutar ./Ejercicio7.exe
